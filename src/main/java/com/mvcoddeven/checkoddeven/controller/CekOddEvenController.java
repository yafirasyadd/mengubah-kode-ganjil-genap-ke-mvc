package com.mvcoddeven.checkoddeven.controller;

import com.mvcoddeven.checkoddeven.model.CekOddEven;
import com.mvcoddeven.checkoddeven.response.CekOddEvenResponse;
import com.mvcoddeven.checkoddeven.service.CekOddEvenService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("")
public class CekOddEvenController {

    private CekOddEvenService cekOddEvenService;

    @RequestMapping("")
    public String CekOddEvenIndex(Model model){
        return "view/index";
    }

    @RequestMapping("/table")
    public String table(CekOddEven cekOddEven, Model model) {
        List<CekOddEvenResponse> oddEvenResponses = new ArrayList<CekOddEvenResponse>();

        oddEvenResponses = cekOddEvenService.getAllNumber(cekOddEven.getNum1(),cekOddEven.getNum2());

        model.addAttribute("allNum", oddEvenResponses);
        return "view/table";
    }



}
