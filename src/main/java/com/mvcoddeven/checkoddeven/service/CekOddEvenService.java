package com.mvcoddeven.checkoddeven.service;
import com.mvcoddeven.checkoddeven.response.CekOddEvenResponse;

import java.util.ArrayList;
import java.util.List;


public class CekOddEvenService {
    public static List<CekOddEvenResponse> getAllNumber(int num1, int num2) {
        List<CekOddEvenResponse> oddEvenResponses = new ArrayList<CekOddEvenResponse>();

        for (int i = 0; i <= num2 - num1; i++) {
            CekOddEvenResponse oddEvenResponse = new CekOddEvenResponse();

            int current = i + num1;
            oddEvenResponse.setNum(current);
            if (current % 2 == 0) {
                oddEvenResponse.setOutput("genap");
            } else {
                oddEvenResponse.setOutput("ganjil");
            }

            oddEvenResponses.add(oddEvenResponse);
        }

        return oddEvenResponses;
    }
}
