package com.mvcoddeven.checkoddeven.response;

public class CekOddEvenResponse {
    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    private int num;
    private String output;
}
